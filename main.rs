use std::fs;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};

// HTTP Response status codes
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
enum StatusCodes {
    Ok,
    NotFound,
}

static SELF_ADDRESS: &'static str = "0.0.0.0:80";

fn main() {
    let listener = TcpListener::bind(SELF_ADDRESS).unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        connection_handler(stream);
    }
}

fn connection_handler(mut stream: TcpStream) {
    let mut buffer = [0; 1024];
    stream.read(&mut buffer).unwrap();

    let get_index = b"GET / HTTP/1.1\r\n";
    let get_favicon = b"GET /favicon.ico HTTP/1.1\r\n";

    if buffer.starts_with(get_index) {
        let content = fs::read_to_string("index.html").unwrap();
        
        let response = format!("HTTP/1.1 200 OK\r\nContent-Length: {}\r\n\r\n{}",
            content.len(), content
        );
        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
        println!("{}", String::from_utf8_lossy(&buffer[..]));
    } else if buffer.starts_with(get_favicon) {
        let response = "HTTP/1.1 404 Not Found\r\n\r\n";

        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
    }
}
